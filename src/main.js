import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
// 导入全局样式
import './assets/css/global.css'
// 导入字体图标
import './assets/fonts/iconfont.css'
import axios from 'axios'
import VueResource from 'vue-resource'
import TreeTable from 'vue-table-with-tree-grid'
// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
// 导入富文本编辑器对应的样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
axios.interceptors.request.use(config => {
  config.headers.Authorization = window.sessionStorage.getItem('token')
  // 在最后必须return config
  return config
})

Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.component('tree-table', TreeTable)
// 将富文本编辑器注册为全局可用的组件
Vue.use(VueQuillEditor)
// 可视化时间的过滤器
Vue.filter('dataFormat', function (originVal) {
  const dt = new Date(originVal)
  const y = dt.getFullYear()
  // padStart第一个参数表示至少要2位 如果不足2位 则用0填充
  const m = (dt.getMonth() + 1 + '').padStart(2, 0)
  const d = (dt.getDate() + 1 + '').padStart(2, 0)
  const hh = (dt.getHours() + 1 + '').padStart(2, 0)
  const mm = (dt.getMinutes() + 1 + '').padStart(2, 0)
  const ss = (dt.getSeconds() + 1 + '').padStart(2, 0)
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})

new Vue({
  VueResource,
  router,
  render: h => h(App)
}).$mount('#app')
